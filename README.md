Sujet 1 : Communication entre iframe
==============================
Vous avez deux domaines différents (domaine1.com, domaine2.com).
Inclure une iframe sur le domaine2 dans une page du domaine1.
Faire en sorte que les deux communiquent de manière sécurisée.
Cet exercice doit être compatible Chrome / Firefox / Safari / IE7+


Installation
------------

### Domain 1
Les fichiers du dossier domain1 sont à placer à la racine du domain1.com :
``domain1.com/parent.html``
``domain1.com/parentProxy.html``

Dans les 2 fichiers il faut définir le domain d'origine :
```javascript
document.domain = "domain1.com";
```

### Domain 2
Les fichiers du dossier domain2 sont à placer à la racine du domain2.com :
``domain2.com/frame.html``
``domain2.com/frameProxy.html``

Dans les 2 fichiers il faut définir le domain d'origine :
```javascript
document.domain = "domain2.com";
```
